# K8s Openvpn

This project is for creating OpenVPN environment on Raspberry Pi Kubernetes based on [chepurko/k8s-ovpn](https://github.com/chepurko/k8s-ovpn) and [kylemanna/docker-openvpn](https://github.com/kylemanna/docker-openvpn).

## Requirements

1. Kubernetest environment on Raspberry Pi.
1. 64bit Ubuntu (I confirmed on [Ubuntu 20.04](https://ubuntu.com/download/raspberry-pi)).
1. Please refer [chepurko/k8s-ovpn](https://github.com/chepurko/k8s-ovpn).

## How to use

This procidure is from [chepurko/k8s-ovpn - Installation](https://github.com/chepurko/k8s-ovpn#installation).

1. Create PKI and server certificate.

    ```
    ./create-openvpn.sh <OPENVPN_HOST>
    ```

1. Create client certificate.

    ```
    ./craete-client.sh <CLIENTNAME>
    ```

1. Copy necessary files to Kubernetes master node.

    * Server certificate exported to ovpn0/server directory.
    * Manufests
        * manufests/Namespace.yaml
        * manufests/Deployment.yaml
 
    ```
    mv ovpn0/server .
    tar zcvf server server.tar.gz
    scp server.tar.gz <Your master node>:.
    scp manufests/Namespace.yaml <Your master node>:.
    scp manufests/Deployment.yaml <Your master node>:.
    ```

1. Extract server certificate.

    **Log into your Kubernetes master node from this step**

    ```
    tar xvf server.tar.gz
    sudo chown -R `id -u`:`id -g` server
    ```

1. Create namespace.

    Create **vpn-system** namespace.

    ```
    kubectl apply -f Namespace.yaml
    ```

1. Create Secrets for OpenVPN

    ```
    export OPENVPN_HOST=<Your OPENVPN_HOST>
    export OPENVPN_NAMESPACE=vpn-system
    kubectl create secret -n $OPENVPN_NAMESPACE generic openvpn0-key --from-file=server/pki/private/$OPENVPN_HOST.key
    kubectl create secret -n $OPENVPN_NAMESPACE generic openvpn0-cert --from-file=server/pki/issued/$OPENVPN_HOST.crt
    kubectl create secret -n $OPENVPN_NAMESPACE generic openvpn0-pki \
        --from-file=server/pki/ca.crt --from-file=server/pki/dh.pem --from-file=server/pki/ta.key
    kubectl create configmap -n $OPENVPN_NAMESPACE openvpn0-conf --from-file=server/
    kubectl create configmap -n $OPENVPN_NAMESPACE ccd0 --from-file=server/ccd
    ```

1. Deploy OpenVPN container.

    ```
    kubectl apply -f Deployment.yaml
    ```

1. Connect usin your VPN client.

    Remember to add VPN_HOSTNAME to your hosts file or register DNS.

    * Window : C:\windows\drivers\etc\hosts
    * Mac : /private/etc/hosts
    * Linux : /etc/hosts

