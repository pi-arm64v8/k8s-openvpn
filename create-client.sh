#!/bin/bash

set -eu

CLIENTNAME=$1

pushd ovpn0

function error() {
    popd
}
trap error ERR

docker run -e EASYRSA_ALGO=ec -e EASYRSA_CURVE=secp384r1 \
    --net=none --rm -it -v $PWD:/etc/openvpn kylemanna/openvpn easyrsa build-client-full $CLIENTNAME
docker run --net=none --rm -v $PWD:/etc/openvpn kylemanna/openvpn ovpn_getclient $CLIENTNAME > $CLIENTNAME.ovpn

VPNCLIENT=$PWD/$CLIENTNAME.ovpn
popd

mv $VPNCLIENT .