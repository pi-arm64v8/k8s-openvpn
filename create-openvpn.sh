#!/bin/bash

set -eu

OPENVPN_HOST=$1

mkdir -p ovpn0
pushd ovpn0

function error() {
    popd
}
trap error ERR

docker run --net=none --rm -it -v $PWD:/etc/openvpn kylemanna/openvpn ovpn_genconfig \
    -u udp://$OPENVPN_HOST:31304 \
    -C 'AES-256-GCM' -a 'SHA384' -T 'TLS-ECDHE-ECDSA-WITH-AES-256-GCM-SHA384' \
    -b -n 10.0.0.1 -n 1.1.1.1 -n 208.67.222.222
docker run -e EASYRSA_ALGO=ec -e EASYRSA_CURVE=secp384r1 \
    --net=none --rm -it -v $PWD:/etc/openvpn kylemanna/openvpn ovpn_initpki
docker run --net=none --rm -it -v $PWD:/etc/openvpn kylemanna/openvpn ovpn_copy_server_files

tar zcvf server.tar.gz server
SERVERFILE=$PWD/server.tar.gz

popd

mv $SERVERFILE .