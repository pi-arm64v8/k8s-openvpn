#!/bin/bash

set -eu

mkdir -p ovpn0
pushd ovpn0

function error() {
    popd
}
trap error ERR

docker run --net=none --rm -it -v $PWD:/etc/openvpn kylemanna/openvpn ovpn_copy_server_files

tar zcvf server.tar.gz server
SERVERFILE=$PWD/server.tar.gz

popd

mv $SERVERFILE .