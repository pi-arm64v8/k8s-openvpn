#!/bin/bash

set -eu

CLIENTNAME=$1

pushd ovpn0

function error() {
    popd
}
trap error ERR

docker run --rm -it -v $PWD:/etc/openvpn kylemanna/openvpn ovpn_revokeclient "$CLIENTNAME" remove

popd
